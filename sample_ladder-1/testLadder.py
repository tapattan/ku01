import unittest
from ladder import play

unit_test = [ [[0,0,0,0,0],[3,2,2],5], 
              [[2,0,0,0,0],[1,1]  ,4],
              [[0,0,0,-10,0],[2,2],0],
              [[0,2,0,-10,0],[1,1],4],
              [[0,2,0,-10,0],[3,6],5], 
              [[0,0,-1,-1,0],[1,1,1,2],3],] #ทดลองสร้างแผนที่ใหม่

class MyTest(unittest.TestCase):
  def test_play(self):
     for i in unit_test:
       m_map = i[0]
       dice = i[1]
       test = i[2]
    
       result = play(m_map,dice)
       
       self.assertEqual(result, test)
 
if __name__ == '__main__':
  unittest.main()
