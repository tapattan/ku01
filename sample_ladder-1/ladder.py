def play(m_map,dice):
    my_pos = 0
    for i in dice:
      my_pos += i # ปรับตำแหน่งการเดินล่าสุด
      
      if(my_pos>=len(m_map)):
        return len(m_map)
    
      extra = m_map[my_pos-1]
      my_pos += extra
    
    
    if(my_pos<0):
        return 0
    return my_pos
