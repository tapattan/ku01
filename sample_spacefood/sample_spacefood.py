def sample_spacefood(row1,row2):
    #n,m,k = [4,2,3]
    #food = [5,2,1,4]
    n,m,k = row1[0],row1[1],row1[2]
    food = row2

    food_stock = 0
    hungry = 0
    dead = 0
    for i in range(len(food)):
       food_stock += food[i] #new food on hand
       #print(food_stock)
       if(food_stock >= k):
         food_stock -=k #ถ้ามีอาหารมากกว่าตอนหิวก็ทานไปก่อน
       else:
         if(food_stock>=m):
            hungry+=1   
            #print('day at',i+1)
            food_stock = 0 # ถ้าน้อยกว่าหิวแต่มากกว่ารอด ให้กินให้หมด 
         else:
            dead = -1 #ตาย

    #สรุปผล
    if(dead==-1):
      return -1 #print(-1)
    else:
      #print(hungry)
      return hungry
