import unittest
from sample_spacefood import sample_spacefood

data = [
         [[4,2,3],[5,2,1,4],1],
         [[4,2,3],[5,1,1,4],-1],
       ]

class MyTest(unittest.TestCase):
  def test_sample_spacefood(self): 
    for i in data:
      row1 = i[0]
      row2 = i[1]
      result = i[2]  
      k = sample_spacefood(row1,row2)
      self.assertEqual(k, result)
 
if __name__ == '__main__':
  unittest.main()
